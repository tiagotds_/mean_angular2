import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  readonly BASE_URL: string = `${environment.apiUrl}/billingSummary`;

  constructor(private http: HttpClient) { }

  getSummary(): Observable<any> {
    return this.http.get(this.BASE_URL);
  }
}
