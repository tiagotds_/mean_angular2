import { Component, OnInit } from '@angular/core';
import { DashboardService } from './dashboard.service';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  credit: number = 0;
  debt: number = 0;
  total: number = 0;

  constructor(private service: DashboardService) { }

  ngOnInit(): void {
    this.getSummary();
  }

  getSummary(): void {
    this.service.getSummary().subscribe(response => {
      const {credit = 0, debt = 0} = response;
      this.credit = credit;
      this.debt = debt;
      this.total = credit - debt;
    });
  }

}
