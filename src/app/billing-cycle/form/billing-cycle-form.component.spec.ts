import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BillingCycleFormComponent } from './billing-cycle-form.component';

describe('BillingCycleFormComponent', () => {
  let component: BillingCycleFormComponent;
  let fixture: ComponentFixture<BillingCycleFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BillingCycleFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BillingCycleFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
