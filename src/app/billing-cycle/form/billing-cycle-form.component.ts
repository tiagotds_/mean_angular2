import { BillingCycleService } from '../billing-cycle.service';
import { Component, OnInit } from '@angular/core';
import { BillingCycle, Credit, Debt } from '../model';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-billing-cycle-form',
  templateUrl: './billing-cycle-form.component.html',
  styleUrls: ['./billing-cycle-form.component.css']
})
export class BillingCycleFormComponent implements OnInit {

  billingCycle: BillingCycle;
  credit: number;
  debt: number;
  id: string;
  private state: {
    url: string,
    page: number
  };

  constructor(
    private service: BillingCycleService,
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService
  ) { 
    const navigation = this.router.getCurrentNavigation();
    this.state = navigation.extras.state as {
      url: string,
      page: number
    };
  }

  ngOnInit(): void {
    this.credit = 0;
    this.debt = 0;
    this.id = this.route.snapshot.params['id'];
    if(this.id) {
      this.get(this.id);
    } else {
      this.init();
    }
  }

  init(): void {
    this.billingCycle = new BillingCycle('', 0, 0, [new Credit('',0)], [new Debt('', 0, null)]);
    this.credit = 0;
    this.debt = 0;
    this.updateValues({});
  }

  get(id: string): void {
    this.service.get(id).subscribe(response => {
      this.billingCycle = response;
      this.updateValues({});
    });
  }

  updateValues({credit = this.credit, debt = this.debt}): void {
    this.credit = credit;
    this.debt = debt;
  }

  updateCredit(credit: number) {
    this.credit = credit;
    this.updateValues({});
  }

  updateDebt(debt: number) {
    this.debt = debt;
    this.updateValues({});
  }

  buttonClass(): {} {
    return {
      'btn-primary': !this.id,
      'btn-info': this.id
    }
  }

  buttonTitle(): string {
    return this.id ? 'Alterar' : 'Incluir';
  }

  save(): void {
    if(this.id){
      this.service.update(this.billingCycle).subscribe(responseSuccess =>{
        this.showSuccess('Alterar');
      },
      responseError => {
        this.showError('Alterar', responseError.error.errors);
      });
    } else {
      this.service.create(this.billingCycle).subscribe(responseSuccess =>{
        this.init();
        this.showSuccess('Incluir');
      },
      responseError => {
        this.showError('Incluir', responseError.error.errors);
      });
    }
    
  }
  
  showSuccess(title: string): void {
    this.toastr.success('Operação realizada com sucesso!', title);
  }

  showError(title: string, errors: Array<string>): void {
    errors.forEach(err => this.toastr.error(err, title));
  }

  back(): void {
    this.router.navigate([this.state.url], {queryParams: {page: this.state.page}});
  }

}
