import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';

import { BillingCycleListComponent } from './list';
import { BillingCycleFormComponent } from './form';
import { CreditListComponent } from './credit-list';
import { DebtListComponent } from './debt-list';
import { SharedModule } from '../shared';
import { BillingCycleRoutingComponent } from './billing-cycle-routing.component';
import { BillingCycleRoutingModule } from './billing-cycle-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { BillingCycleService } from './billing-cycle.service';

@NgModule({
  declarations: [
    BillingCycleListComponent, 
    BillingCycleFormComponent,
    BillingCycleRoutingComponent,
    CreditListComponent,
    DebtListComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    HttpClientModule,
    SharedModule,
    BillingCycleRoutingModule,
    BrowserAnimationsModule, 
    ToastrModule.forRoot(), 
  ],
  providers: [
    BillingCycleService
  ]
})
export class BillingCycleModule { }
