import { Component, OnInit, EventEmitter } from '@angular/core';
import { Status, Debt } from '../model';

@Component({
  selector: 'debt-list',
  inputs: [ "debts" ],
	outputs: [
		"debtsChangeEmmiter: debtsChange",
		"totalEmitter: debt"
	],
  templateUrl: './debt-list.component.html',
  styleUrls: ['./debt-list.component.css']
})
export class DebtListComponent implements OnInit {

  status = Status;
  debts: Array<Debt>;
  debtsChangeEmmiter: EventEmitter<Array<Debt>> = new EventEmitter();
  totalEmitter: EventEmitter<number> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
    this.calculateValues();
  }

  calculateValues(): void {
    let debt: number = 0;
    this.debts.forEach((d: Debt) => {
        debt += d.value;
    });
    this.totalEmitter.emit(debt);
  }

  add(index: number): void {
    this.debts.splice(index + 1, 0, new Debt('', 0, null));
    this.calculateValues();
  }

  clone(index: number, debt: Debt): void {
    this.debts.splice(index + 1, 0, new Debt(debt.name, debt.value, debt.status));
    this.calculateValues();
  }

  delete(index: number){
    if(this.debts.length > 1){
      this.debts.splice(index, 1);
    } else {
      this.debts = [new Debt('', 0, null)];
      this.debtsChangeEmmiter.emit(this.debts);
    }
    this.calculateValues();
  }

}
