import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BillingCycleListComponent } from './billing-cycle-list.component';

describe('BillingCycleListComponent', () => {
  let component: BillingCycleListComponent;
  let fixture: ComponentFixture<BillingCycleListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BillingCycleListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BillingCycleListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
