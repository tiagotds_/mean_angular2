import { Component, OnInit } from '@angular/core';
import { BillingCycleService } from '../billing-cycle.service';
import { BillingCycle } from '../model';
import { ActivatedRoute, Router, Params, NavigationExtras } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-billing-cycle-list',
  templateUrl: './billing-cycle-list.component.html',
  styleUrls: ['./billing-cycle-list.component.css']
})
export class BillingCycleListComponent implements OnInit {

  billingCycles: BillingCycle[];
  pages: number;
  page: number;
  url: string;

  constructor(
    private service: BillingCycleService,
    private router: Router,
    private route: ActivatedRoute,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.url = this.router.url.split('?')[0];

    this.route.queryParams.subscribe((params: Params) => {
      this.page = +params['page'] || 1;
      this.getPages();
      this.refresh(this.page);

    });

  }

  refresh(page: number): void {
    this.service.refresh(page).subscribe(response => {
      this.billingCycles = response;
    });
  }

  remove($event: any, billingCycle: BillingCycle): void {
    $event.preventDefault();
    if(confirm(`Deseja remover o ciclo de pagamento "${billingCycle.name}"?`)){
      this.service.delete(billingCycle._id).subscribe(response => {
        this.showSuccess('Remover');
        this.ngOnInit();
      },
      responseError => {
        this.showError('Incluir', responseError.error.errors);
      });
    }
  }

  showSuccess(title: string): void {
    this.toastr.success('Operação realizada com sucesso!', title);
  }

  showError(title: string, errors: Array<string>): void {
    errors.forEach(err => this.toastr.error(err, title));
  }

  getPages(): void {
    this.service.count().subscribe(response => {
      const pages = Math.ceil(response.value / this.service.countByPage());
      this.pages = pages;
    },
    error => {
      this.pages = 1;
    })
  }

  goToForm(id?: string): void {
    let route = ['/billing-cycle'];
    if(id) route.push(id);
    const navigationExtras: NavigationExtras = {
      state: { 
        url: this.url, 
        page: this.page
      }
    };
    this.router.navigate(route, navigationExtras);
  }


}
