export class Credit {
    constructor(
        public name: string,
        public value: number
    ){}
}