export enum Status {
    AGENDADO = 'Agendado',
    PAGO = 'Pago',
    PENDENTE = 'Pendente'
}