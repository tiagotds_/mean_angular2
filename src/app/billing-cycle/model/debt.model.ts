import { Status } from './debt-status.model';

export class Debt {
    constructor(
        public name: string,
        public value: number,
        public status: Status
    ) {}
}