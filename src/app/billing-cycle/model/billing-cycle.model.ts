import { Credit } from './credit.model';
import { Debt } from './debt.model';

export class BillingCycle {
    constructor(
        public name: string,
        public month: number,
        public year: number,
        public credits: Credit[],
        public debts: Debt[],
        public _id?: string,
    ) {}
}