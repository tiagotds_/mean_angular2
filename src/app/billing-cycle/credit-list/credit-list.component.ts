import { Component, OnInit, EventEmitter } from '@angular/core';
import { Credit } from '../model';

@Component({
  selector: 'credit-list',
  inputs: [ "credits" ],
	outputs: [
		"creditsChangeEmmiter: creditsChange",
		"totalEmitter: credit"
	],
  templateUrl: './credit-list.component.html',
  styleUrls: ['./credit-list.component.css']
})
export class CreditListComponent implements OnInit {

credits: Array<Credit>;
creditsChangeEmmiter: EventEmitter<Array<Credit>> = new EventEmitter();
totalEmitter: EventEmitter<number> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
    this.calculateValues();
  }

  calculateValues(): void {
    let credit: number = 0;
    this.credits.forEach((c: Credit) => {
        credit += c.value;
    });
    this.totalEmitter.emit(credit);
  }

  add(index: number): void {
    this.credits.splice(index + 1, 0, new Credit('', 0));
    this.calculateValues();
  }

  clone(index: number, credit: Credit): void {
    this.credits.splice(index + 1, 0, new Credit(credit.name, credit.value));
    this.calculateValues();
  }

  delete(index: number){
    if(this.credits.length > 1){
      this.credits.splice(index, 1);
    } else {
      this.credits = [new Credit('', 0)];
      this.creditsChangeEmmiter.emit(this.credits);
    }
    this.calculateValues();
  }



}
