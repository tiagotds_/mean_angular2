import { TestBed } from '@angular/core/testing';

import { BillingCycleService } from './billing-cycle.service';

describe('BillingCycleService', () => {
  let service: BillingCycleService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BillingCycleService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
