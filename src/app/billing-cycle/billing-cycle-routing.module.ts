import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { BillingCycleRoutingComponent } from './billing-cycle-routing.component';
import { BillingCycleListComponent } from './list';
import { BillingCycleFormComponent } from './form';

export const BillingCycleRoutes: Routes = [
    {
        path: 'billing-cycle',
        component: BillingCycleRoutingComponent,
        children: [
            {
                path: 'list',
                component: BillingCycleListComponent
            },
            {
                path: '',
                component: BillingCycleFormComponent
            },
            {
                path: ':id',
                component: BillingCycleFormComponent
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(BillingCycleRoutes)],
    exports: [RouterModule]
})
export class BillingCycleRoutingModule {}