import { Component } from "@angular/core";

@Component({
    template: `
    <content-header title="Ciclo de Pagamentos" subtitle="Cadastro"></content-header>
    <section class="content">
        <router-outlet></router-outlet>
    </section>
    `
})
export class BillingCycleRoutingComponent {}