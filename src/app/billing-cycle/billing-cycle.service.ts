import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BillingCycle } from './model';

@Injectable({
  providedIn: 'root'
})
export class BillingCycleService {

  private readonly BASE_URL: string = `${environment.apiUrl}/billingCycles`;
  private readonly COUNT: number = 5;

  constructor(private http: HttpClient) { }

  refresh(page: number): Observable<any> {
    return this.http.get(`${this.BASE_URL}?skip=${(page-1) * this.COUNT}&limit=${this.COUNT}`);
  }

  get(id: string): Observable<any> {
    return this.http.get(`${this.BASE_URL}/${id}`);
  }

  create(billingCycle: BillingCycle): Observable<any> {
    return this.http.post(`${this.BASE_URL}`, billingCycle);
  }

  update(billingCycle: BillingCycle): Observable<any> {
    return this.http.put(`${this.BASE_URL}/${billingCycle._id}`, billingCycle);
  }

  delete(id: string): Observable<any> {
    return this.http.delete(`${this.BASE_URL}/${id}`);
  }

  count(): Observable<any> {
    return this.http.get(`${this.BASE_URL}/count`);
  }

  countByPage(): number {
    return this.COUNT;
  }
}
