import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent, MenuComponent, FooterComponent, ValueBoxComponent, ContentHeaderComponent, FieldComponent, PaginatorComponent } from './components';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from '../app-routing.module';
import { SharedService } from './shared.service';

@NgModule({
  declarations: [
    HeaderComponent,
    MenuComponent,
    FooterComponent,
    ValueBoxComponent,
    ContentHeaderComponent,
    FieldComponent,
    PaginatorComponent
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    FormsModule
  ],
  exports: [
    HeaderComponent,
    MenuComponent,
    FooterComponent,
    ValueBoxComponent,
    ContentHeaderComponent,
    FieldComponent,
    PaginatorComponent,
    CommonModule, 
    FormsModule
  ],
  providers: [
    SharedService
  ]
})
export class SharedModule { }
