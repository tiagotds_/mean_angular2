import { Component, OnInit, Input, forwardRef } from '@angular/core';
import { SharedService } from '../../shared.service';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';


@Component({
  selector: 'field',
  templateUrl: './field.component.html',
  styleUrls: ['./field.component.css'],
  providers: [
    {
       provide: NG_VALUE_ACCESSOR,
       useExisting: forwardRef(() => FieldComponent),
       multi: true
    }
  ]
})
export class FieldComponent implements OnInit, ControlValueAccessor  {

  @Input() id: string;
  @Input() grid: string;
  @Input() label: string;
  @Input() placeholder: string;
  @Input() type?: string = 'text';

  private _value: any;
  private onChange: (_: any) => void = () => {};
  private onTouched: () => void = () => {};
  
  constructor(private shared: SharedService) { }
  
  ngOnInit(): void {
  }
  
  registerOnChange(fn: any): void {
    this.onChange = fn;
  }
  
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }
  
  writeValue(obj: any): void {
    this.value = obj;
  }

  gridSystemClasses(): string {
    return this.shared.gridSystemClasses(this.grid);
  }

  public get value(): any{
    return this._value;
  }
  
  public set value(v) {
    if(v !== this._value){
      this._value = v;
      this.onChange(this._value);
    }
  }
  
}
