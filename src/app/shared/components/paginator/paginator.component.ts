import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.css']
})
export class PaginatorComponent implements OnInit {

  @Input() url: string;
  private _pages = new BehaviorSubject<number>(1);
  pagesArray: Array<number>;
  current: number;
  needPagination: boolean;

  constructor(
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this._pages.subscribe(response => this.init());
  }

  init(): void {
    const pages = this.pages || 1;
    this.route.queryParams.subscribe((params: Params) => {
      const page = +params['page'] || 1;
      this.pagesArray = Array(pages).fill(0).map((e, i) => i + 1);
      this.current = page || 1;
      this.needPagination = this.pages > 1;
    })
  }

  @Input()
  set pages(value) {
      this._pages.next(value);
  };

  get pages() {
      return this._pages.getValue();
  }

  hasPrev(): boolean {
    return this.current > 1;
  }

  hasNext(): boolean {
    return this.current < this.pages;
  }

  isCurrent(page: number): boolean {
    return this.current == page;
  }

  goToPage(page: number): void {
    this.router.navigate([`${this.url}`], {queryParams: {page: page}})
  }

  getClass(index: number): {} {
    return {
      'active': this.isCurrent(index)
    }
  }

}
