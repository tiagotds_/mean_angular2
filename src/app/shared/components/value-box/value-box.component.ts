import { Component, OnInit, Input } from '@angular/core';
import { SharedService } from '../../shared.service';

@Component({
  selector: 'value-box',
  templateUrl: './value-box.component.html',
  styleUrls: ['./value-box.component.css']
})
export class ValueBoxComponent implements OnInit {

  @Input() grid: string;
  @Input() value: number;
  @Input() text: string;
  @Input('color-class') colorClass: string;
  @Input('icon-class') iconClass: string;

  constructor(private shared: SharedService) { }

  ngOnInit(): void {

  }

  gridSystemClasses(): string {
    return this.shared.gridSystemClasses(this.grid);
  }
  



}
